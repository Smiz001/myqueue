﻿namespace MyQuare
{
  partial class Main
  {
    /// <summary>
    /// Обязательная переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором форм Windows

    /// <summary>
    /// Требуемый метод для поддержки конструктора — не изменяйте 
    /// содержимое этого метода с помощью редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.btPush = new System.Windows.Forms.Button();
      this.btPop = new System.Windows.Forms.Button();
      this.lbQueue = new System.Windows.Forms.ListBox();
      this.SuspendLayout();
      // 
      // btPush
      // 
      this.btPush.Location = new System.Drawing.Point(202, 12);
      this.btPush.Name = "btPush";
      this.btPush.Size = new System.Drawing.Size(75, 23);
      this.btPush.TabIndex = 0;
      this.btPush.Text = "Push";
      this.btPush.UseVisualStyleBackColor = true;
      this.btPush.Click += new System.EventHandler(this.btPush_Click);
      // 
      // btPop
      // 
      this.btPop.Location = new System.Drawing.Point(317, 12);
      this.btPop.Name = "btPop";
      this.btPop.Size = new System.Drawing.Size(75, 23);
      this.btPop.TabIndex = 1;
      this.btPop.Text = "Pop";
      this.btPop.UseVisualStyleBackColor = true;
      this.btPop.Click += new System.EventHandler(this.btPop_Click);
      // 
      // lbQueue
      // 
      this.lbQueue.FormattingEnabled = true;
      this.lbQueue.Location = new System.Drawing.Point(12, 12);
      this.lbQueue.Name = "lbQueue";
      this.lbQueue.Size = new System.Drawing.Size(184, 238);
      this.lbQueue.TabIndex = 2;
      // 
      // Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(404, 268);
      this.Controls.Add(this.lbQueue);
      this.Controls.Add(this.btPop);
      this.Controls.Add(this.btPush);
      this.Name = "Main";
      this.Text = "Очередь";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btPush;
    private System.Windows.Forms.Button btPop;
    private System.Windows.Forms.ListBox lbQueue;
  }
}

