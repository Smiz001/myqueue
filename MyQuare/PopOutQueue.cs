﻿using System.Collections;

namespace MyQuare
{
  public class PopOutQueue
  {
    #region Properties

    public Queue ThisQueue { get; set; }

    #endregion

    public PopOutQueue(Queue queue)
    {
      ThisQueue = queue;
    }

    public T Pop<T>()
    {
      while (ThisQueue.Count == 0)
      {
        
      }
      return (T)ThisQueue.Dequeue();
    }
  }
}