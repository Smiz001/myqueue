﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace MyQuare
{
  public partial class Main : Form
  {
    private readonly Queue m_queueInt = new Queue();
    private PushInQueue m_pushInQueue;
    private PopOutQueue m_popOutQueue;
    private readonly Random m_random = new Random();

    public Main()
    {
      InitializeComponent();
    }

    private void btPush_Click(object sender, System.EventArgs e)
    {
      if(m_pushInQueue == null)
        m_pushInQueue = new PushInQueue(m_queueInt);
      var i = m_random.Next(0, 1000);
      Thread thread = new Thread(new ParameterizedThreadStart(m_pushInQueue.Push));
      thread.Start(i);
      lbQueue.Items.Add(i);
    }

    private void btPop_Click(object sender, System.EventArgs e)
    {
      if(m_popOutQueue == null)
        m_popOutQueue = new PopOutQueue(m_queueInt);
      Thread thread = new Thread(new ThreadStart(Pop));
      thread.Start();
      /*
      try
      {
        var val = m_popOutQueue.Pop<int>();
        lbQueue.Items.Remove(val);
        MessageBox.Show(val.ToString());
      }
      catch (Exception ex)
      {
        MessageBox.Show("Список пуст");
      } 
      */
    }

    private void Pop()
    {
      var val = m_popOutQueue.Pop<int>();
      lbQueue.Items.Remove(val);
      MessageBox.Show(val.ToString());
    }
  }
}
