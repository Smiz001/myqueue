﻿using System.Collections;
using System.Collections.Generic;

namespace MyQuare
{
  public class PushInQueue
  {
    #region Properties

    public Queue ThisQueue { get; set; }

    #endregion

    public PushInQueue(Queue queue)
    {
      ThisQueue = queue;
    }

    public void Push<T>(T value)
    {
      ThisQueue.Enqueue(value);
    }
  }
}